#!/bin/bash
set -Eeuo pipefail
if [ "${1-}" != "" ]; then
    STACK_NAME="${1}"
else
    echo "Specify the name of the CloudFormation stack to connect to"
    exit 1
fi
ENVIRONMENT_NAME=$(aws cloudformation describe-stack-resource --stack-name "${STACK_NAME}" --logical-resource-id BeanstalkEnvironment --output text --query 'StackResourceDetail.PhysicalResourceId')
INSTANCE_ID=$(aws elasticbeanstalk describe-environment-resources --environment-name "${ENVIRONMENT_NAME}" --output text --query 'EnvironmentResources.Instances[0].Id')
echo "Connecting to instance ${INSTANCE_ID}"
aws ssm start-session --target "${INSTANCE_ID}"
