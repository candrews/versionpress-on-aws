---
variables:
  S3_BUCKET_NAME: "versionpress-on-aws"
  AWS_DEFAULT_REGION: "us-east-1"

stages:
  - build
  - test
  - deploy

build:
  stage: build
  image:
    name: python:buster
  script:
    - apt-get update -qq && apt-get install -y jq shellcheck zip
    - pip install cfn_flip yq
    - ./build.sh
  artifacts:
    paths:
      - target/beanstalk-app.zip
      - target/beanstalk-app-s3-url
      - target/cloudformation.json

lint_ebextensions:
  stage: test
  image: sdesbure/yamllint
  script:
    - yamllint -s ./beanstalk/.ebextensions/*.config .

lint_dockerfile:
  stage: test
  image: hadolint/hadolint:latest-alpine
  script:
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate "beanstalk/Dockerfile" > reports/hadolint-$(md5sum "beanstalk/Dockerfile" | cut -d" " -f1).json
  artifacts:
    when: always
    reports:
      codequality:
        - "reports/*"

cfn_lint:
  stage: test
  image: python:3-alpine
  script:
    - pip install cfn-lint
    - cfn-lint target/cloudformation.json

validate_cloudformation:
  only:
    variables:
      - $AWS_ACCESS_KEY_ID
      - $AWS_SECRET_ACCESS_KEY
  stage: test
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  script:
    - aws cloudformation validate-template --template-body file://target/cloudformation.json

cfn_nag:
  stage: test
  image:
    name: stelligent/cfn_nag
    entrypoint: [""]
  script:
    - cfn_nag cloudformation.yml

deploy:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  script:
    - aws s3 cp target/beanstalk-app.zip "$(cat target/beanstalk-app-s3-url)"
    - aws s3 cp target/cloudformation.json s3://${S3_BUCKET_NAME}/cloudformation.json
  only:
    - master

include:
  - template: Code-Quality.gitlab-ci.yml
