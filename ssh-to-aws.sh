#!/bin/bash
set -Eeuo pipefail
if [ "${1-}" != "" ]; then
    STACK_NAME="${1}"
else
    echo "Specify the name of the CloudFormation stack to connect to"
    exit 1
fi
RDS_IDENTIFIER=$(aws cloudformation describe-stack-resource --stack-name "${STACK_NAME}" --logical-resource-id Database --output text --query 'StackResourceDetail.PhysicalResourceId')
RDS_SECRET_IDENTIFIER=$(aws cloudformation describe-stack-resource --stack-name "${STACK_NAME}" --logical-resource-id DatabaseMasterSecret --output text --query 'StackResourceDetail.PhysicalResourceId')
RDS_ADDRESS=$(aws rds describe-db-instances --db-instance-identifier "${RDS_IDENTIFIER}" --output text --query 'DBInstances[0].Endpoint.Address')
RDS_PORT=$(aws rds describe-db-instances --db-instance-identifier "${RDS_IDENTIFIER}" --output text --query 'DBInstances[0].Endpoint.Port')
ENVIRONMENT_NAME=$(aws cloudformation describe-stack-resource --stack-name "${STACK_NAME}" --logical-resource-id BeanstalkEnvironment --output text --query 'StackResourceDetail.PhysicalResourceId')
INSTANCE_ID=$(aws elasticbeanstalk describe-environment-resources --environment-name "${ENVIRONMENT_NAME}" --output text --query 'EnvironmentResources.Instances[0].Id')

SSH_PUBLIC_KEY_PATH="${HOME}/.ssh/id_rsa.pub"
SSH_USER="ec2-user"
SSH_PUBLIC_KEY_TIMEOUT=10
SSH_PUBLIC_KEY_PATH="${HOME}/.ssh/id_rsa.pub"
# Try to get an public ssh key from 'ssh agent'
SSH_PUBLIC_KEY="$(keys="$(ssh-add -L)" && echo "$keys" | head -1)"
if [[ -n "${SSH_PUBLIC_KEY}" ]]; then
  SSH_PUBLIC_KEY_SOURCE='ssh agent'
else
  # Try read public ssh key from '${SSH_PUBLIC_KEY_PATH}'
  SSH_PUBLIC_KEY="$([[ -e "${SSH_PUBLIC_KEY_PATH}" ]] && cat "${SSH_PUBLIC_KEY_PATH}")"
  if [[ -n "${SSH_PUBLIC_KEY}" ]]; then
    SSH_PUBLIC_KEY_SOURCE="${SSH_PUBLIC_KEY_PATH}"
  else
    echo "No ssh key present in ssh agent nor at ${SSH_PUBLIC_KEY_PATH}"
    exit 1
  fi
fi
SSH_PUBLIC_KEY="$(cat "${SSH_PUBLIC_KEY_PATH}")"
echo "Temporary add your public ssh key from '${SSH_PUBLIC_KEY_SOURCE}' to authorized_keys on target instance ${INSTANCE_ID}"
aws ssm send-command \
  --instance-ids "${INSTANCE_ID}" \
  --document-name 'AWS-RunShellScript' \
  --parameters commands="\"
    cd ~${SSH_USER}/.ssh || exit 1
    grep -F '${SSH_PUBLIC_KEY}' authorized_keys || echo '${SSH_PUBLIC_KEY} ssm-session' >> authorized_keys
    sleep ${SSH_PUBLIC_KEY_TIMEOUT}
    grep -v -F '${SSH_PUBLIC_KEY}' authorized_keys > .tmp.authorized_keys
    mv .tmp.authorized_keys authorized_keys
  \"" \
  --comment "Grant ssh access for ${SSH_PUBLIC_KEY_TIMEOUT} seconds" > /dev/null

echo "Connecting to instance ${INSTANCE_ID}"
echo "Connect to localhost:13306 to access the database."
echo "To get the database username/password credentials, run: "
echo "aws secretsmanager get-secret-value --region $(aws configure get region) --secret-id ${RDS_SECRET_IDENTIFIER} | jq --raw-output '.SecretString' | jq -r '(\"username: \"+.username,\"password: \"+.password)'"
ssh -o "StrictHostKeyChecking no" -oProxyCommand="sh -c \"aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters 'portNumber=%p'\"" "${SSH_USER}"@"${INSTANCE_ID}" -L13306:"${RDS_ADDRESS}":"${RDS_PORT}"
