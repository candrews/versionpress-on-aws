# WordPress with VersionPress on Amazon Web Services
Quickly setup and easily maintain a [WordPress](https://wordpress.org/) site hosted on AWS with the help of [VersionPress](https://versionpress.com/open-source/).

Using WordPress with VersionPress on Amazon Web Services is a great way to develop and host a WordPress site whether the project is big or small with a team or an individual developer:
* All code (plugins, themes, WordPress core itself) and content (pages, posts, comments, configuration) are stored in git
* Wipe out and recreate the environment at any time without data loss - everything is in git
* Need a staging site, or a new site to test work in progress? Create a new branch and launch a new stack, be up and running in minutes
* Run the exact same site with the same content locally so you can reproduce issues in production effortlessly - no more "works on my machine" situations

See the [VersionPress site](https://versionpress.com/open-source/) for more information.

# Hosting with AWS
Need a small, cheap staging site, but also a full fledged scalable production site with a CDN?
Use the same stack for both - simply specify different parameter values.
Change parameter values whenever you want without downtime or data loss.
For example, when starting out, leave the CloudFront CDN off to save money. When the site becomes popular, add the CloudFront CDN to better handle the load and improve performance for end users.

AWS features leveraged include:
* [Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) for dynamic scalability, self healing, and rolling deployments
* Multiple available zones within a region for high availability
* [Amazon RDS for MariaDB](https://aws.amazon.com/rds/mariadb/) as a low maintenance, high performance, highly scalable database
* [Elastic File System](https://aws.amazon.com/efs/) for file storage shared among any number of servers as scaling demands
* [Route 53](https://aws.amazon.com/route53/) for DNS management
* [AWS Certificate Manager](https://aws.amazon.com/certificate-manager/) for HTTPS certificate management
* [CloudFormation](https://aws.amazon.com/cloudformation/) for deployment and updating of the stack
* Optional [CloudFront](https://aws.amazon.com/cloudfront/) CDN

## Getting started on AWS
1. You must have a git repository to use for the site.
   The repository can be empty - in that case, VersionPress on AWS will automatically install WordPress on first run. If the repository already contains a WordPress site, it will be deployed (in this case, before deploying, you should probably test it locally first. See [Running Locally](#running-locally)).
   You can use any git repository that's accessible from AWS, including GitHub, BitBucket, GitLab, self-hosted, corporate, etc and it can be public or private.
2. You must have an AWS account ([sign up](https://portal.aws.amazon.com/gp/aws/developer/registration/index.html) of you don't already have one)
3. Setup [Route 53](https://console.aws.amazon.com/route53/) for the domain your site will use. See [Getting Started with Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/getting-started.html) for details.
4. Set up a certificate for your domain in [AWS Certificate Manager](https://console.aws.amazon.com/acm/). You can [request a new one](https://docs.aws.amazon.com/acm/latest/userguide/gs-acm-request-public.html) or [import an existing one](https://docs.aws.amazon.com/acm/latest/userguide/import-certificate.html).
5. Click [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://console.aws.amazon.com/cloudformation/home#/stacks/new?stackName=VersionPress&templateURL=https://s3.amazonaws.com/versionpress-on-aws/cloudformation.json) and fill in the resulting form with the appropriate values.
   Notes on some of the parameters:
   - `VersionPressPullSecret` is optional. See [Automatic Remote Updates](#automatic-remote-updates) for its use.
   - `WordpressEnvironment` is for your use. It sets a constant named `WP_ENV` in `wp-config.php` with this value. For example, in production, you could set `WordpressEnvironment` to `production` then check that `WP_ENV=='production'` from a WordPress theme or plugin to have environment conditional behavior.
   - If the provided `GitUrl` uses the `http://` or `https://` protocol:
     - You must include both the username and the password in `GitUrl`
     - Do not specify values for the `KnownHosts` or `SshPrivateKey` parameters
   - If the provided `GitUrl` uses the `ssh://` protocol:
     - fill in the `KnownHosts` parameter with the output of running `ssh-keyscan <host>` where `<host>` is the host from the `GitUrl`. The whole value must be on line; replace new lines with `\\n`. for example:
      ```
      # gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.6\\ngitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9\\n# gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.6\\ngitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=\\n# gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.6\\ngitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf\\n
      ```
      - fill in the `SshPrivateKey` parameter with the SSH private key to use to authenticate to `GitUrl`. The whole value must be on line; replace new lines with `\\n`. For example:
      ```
      -----BEGIN OPENSSH PRIVATE KEY-----\\nb3BlbnNzaC1rZXktdjEAAAAABG5vbmAAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn\\nNhAAAAAwEAAQAAAAEAyZb4KKppd2mRNTreA6v2M3tI6fS+MoWVMez83Xba5GdZtkAdfot7\\n+HtUA5iQdFrzMIUA3E3BEBHR69GQLHw1tEJJNoMfOCVJQjEy3lDjaxwnSs6lAI98ID6c8o\\nXew1JUNg5z57atbjqKNXa2V4i6c23EbbWLPLz5Wq1cfxddOvU2oTEkplHCcBi85qb0EGKo\\nv6c71gD9tskmbtCpoLMFXGAvaV6Z2vh4RTKvGiG973DrLHFrBpjONuCwK0yYN85k8T/WZM\\nf+suF/9p6UF8gOTOilTZKiR9MfbloDUtJ4rWIsVuzHS8MNZhhstclmtIff+aoJzvC0W2uo\\nYBZIaFqVVwAAA9DHV6CIx1egiAAAAAdzc2gtcnNhAAABAQDDlvgoqml3aZE1Ot4zq/Yze0\\njp9L4yhZUxQPzddtrkZ1m2QB1+i3v4e1QDmJB0WvMwhQDcTcEQEdHr0ZAsfDW0Qkk2gx84\\nJUlCMTLeUONrHCdKzqUoj3wgPpzyhd7DUlQ2DnPntq1uOoo1drZXiLpzbcRttYs8vPlarV\\nx/F1069TahMSSmUcJwGLzmpvQQYqi/pzvWAP22ySZu04mgswVcYC9pXpna+HhFMq8aIb3v\\ncOsscWsGmM424LArTJg3zmTxP9Zkx/6y4X/2npQXyA5M6KVNkqJH0x9uWgNS0nitYixW7M\\ndLww1mGGy1yWa0h9/5qgnO8LRba6hgFkhoWpVXAAAAAwEAAQAAAQBvnVUdPu1SHnM1o7hv\\np6CTdIk0cDv2wRuQHwUKocokeEylqe4qqJQlSjOEIi6pHcpUbO/DCpIAkb1G8ir/Vm5ttd\\n3Pft4EOjDLo23kxaiFH3tp4exz/q3dNI21ggWK48SYgXwPKYfw5hyr6WSgJqYFTxtZGxFS\\nudbeK+pwu8D06OCcsHdz1H4LROtYxKZlWSNKOiEVnBe3aM4OJwft0yx1X/67q8hF0zcygf\\n871JEt+kGpHSNAiif0FMXbyGw4ORP0gRCZXzT9maDiTuepy2KILMzqOPjBwdNcN437bDSZ\\n0uGepsnb2l810bVvy9XZgpRcRGggYFv8drRySPS1bBFhAAAAgCfip4k3rmqNq5DZ+fqunX\\nf2xjsU22dKt5vLmw9QBRFH7GL58BE3R3ZSdFqgVwI+Ae4gXSGMKIvbLVKCAhX71R0MBAsq\\nvJuG9HVz+asLoXx8Ye9cPVOctbZVbfRkKkhvTx0mhQBvIxdiwunKeY/l4VtHqLAKA6gi/5\\nh6kqN0qiQbAAAAgQDjpXZ71+O5QezDD8Ce6M9R9PsOA+0tZ+N1zfi5RnZQ+MYvgTxiuUDU\\n5gHDU6PM26mrtKATdYF3uzL/KHiC2Mw6wp35SFEikWa/iHMdZJ5Z9/2W/xMcYyyHrvXq5F\\nAu/V3wHkZJPHADER8A11nl4P8er0Mrm8pfC1vUpgJQTgd5bwAAAIEA4rKwAVhmJjFE6xUc\\nfkHk61VddqUSaE5UQrtc5eQOmtiLBy0EAYe1fHjgkM8AdAeMfvQ0/DS6/2hpvM2d/9WjIb\\n9L05VyJQD+XhVOQmq6bf7vkD1TsKfjcR0h2NSYKIrst7PJWt1b2ggYihilHC7zXz9cv4wN\\n4LqlE9mz+JDYHpkAAAAUY2FuZHJld3NAY3JhaWdhdHdvcmsBAgMEBQBH\\n-----END OPENSSH PRIVATE KEY-----
      ```

That's it! The site is available at `https://<DomainName>`

Every change made on the WordPress site results in a commit made to git by VersionPress. Each commit is automatically pushed back to the git repository.

## Automatic Remote Updates
Using the setup above in [Getting started on AWS](#getting-started-on-aws), every change made on the AWS Wordpress site results in a commit that's pushed back to the git repository. However, changes made by other means (such as by developers) that are pushed to the git repository do not cause the AWS Wordpress site to git pull those changes. To make that work some additional setup is required.

1. Set `VersionPressPullSecret` to a random, secret value.
2. Configure your git server to make an HTTP POST request to `https://<host>/wp-content/vp-pull.sh` with POST parameter named `SECRET` set to the value used in the `VersionPressPullSecret` whenever the appropriate branch is updated.

For example, if you're using GitLab to host the repository, add a [protected variable](https://docs.gitlab.com/ee/ci/variables/#protected-variables) named `VP_PULL_SECRET` with the value used when launching in AWS then add the following as `.gitlab-ci.yml` to your repository after making the appropriate edits (such as replacing `<HOST>`):
```yml
stages:
  - deploy

deploy:
  stage: deploy
  image:
    name: curlimages/curl
    entrypoint: [""]
  script:
  - "exec 3>&1 && HTTP_STATUS=$(curl -w \"%{http_code}\" -o /dev/fd/3 -sSL -H 'Content-Type: application/x-www-form-urlencoded' -d \"SECRET=${VP_PULL_SECRET}\" 'https://<HOST>/wp-content/vp-pull.php') && [ \"$HTTP_STATUS\" = \"200\" ] || { echo \"Failed; HTTP Status: $HTTP_STATUS\" ; exit 1; }"
  only:
  - master
```

## Running locally
You can run your site locally using Docker. This approach will use the same Docker container as is used on AWS. When running in this environment, the `wp-config.php` constant `WP_ENV` is always set to `development` (equivalent to launching the AWS stack with the `WordpressEnvironment` parameter set to `development`).

From the git working copy of your WordPress site, run these commands:
```shell
git config submodule.recurse true
git submodule add https://gitlab.com/candrews/versionpress-on-aws.git aws
```
You probably want to commit that change.

Start docker by running:
`./aws/docker_dev.sh`

Use Ctrl-C to exit. The site will be running at http://localhost:8000

To get a shell inside of the running docker container (to use, for instance, to use the `wp` cli), with `docker_dev.sh` running, fom the `aws` directory run: `docker-compose exec --user www-data wordpress bash`

## Connecting to the AWS instance
If for some reason you need to get a shell on AWS server, you can do so using either `connect-to-aws.sh` or `ssh-to-aws.sh`.

1. Install the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html).
2. [Setup a profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) with your AWS API credentials.

To get a shell only, run `AWS_PROFILE=<profile> connect-to-aws.sh VersionPress` (where "VersionPress" is the name of the CloudFormation stack; "VersionPress" is the default).

To get an SSH shell with port forwarding to the WordPress database (port 13306 on your system will be forwarded to the WordPress database running on AWS), [install the Session Manager Plugin for the AWS CLI ](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html) then run `AWS_PROFILE=<profile> ssh-to-aws.sh VersionPress`

