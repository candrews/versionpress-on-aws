#!/bin/sh
# This is a wrapper so that wp-cli can run as the www-data user so that permissions
# remain correct
sudo -E -u www-data "${WP_CLI_PHP-php}" /bin/wp-cli.phar "${WP_CLI_PHP_ARGS}" "$@"
