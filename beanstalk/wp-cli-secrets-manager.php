<?php
if ( empty ( getenv('WORDPRESS_DB_SECRET_ID') ) ) {
  // if the secret id isn't set, don't install this approach
  return;
}

class aws_secrets_manager_utility {

 /**
  * Database user name
  *
  * @var string
  */
  public $dbuser;

 /**
  * Database password
  *
  * @var string
  */
  public $dbpassword;

  public function __construct() {
    exec('aws secretsmanager get-secret-value --secret-id ' . escapeshellarg(getenv('WORDPRESS_DB_SECRET_ID')) . ' --query SecretString --output text', $retArr, $status);
    if ( $status != 0 ) {
      die("Could not retrieve the AWS Secrets Manager secret");
    }else{
      $data = json_decode ( $retArr[0] );
      $this->dbuser = $data->username;
      $this->dbpassword = $data->password;
    }
  }
}

$aws_secrets_manager_utility = new aws_secrets_manager_utility();
# These constants have to be defined here before WP-CLI loads wp-config.php
define('DB_USER', $aws_secrets_manager_utility->dbuser);
define('DB_PASSWORD', $aws_secrets_manager_utility->dbpassword);
