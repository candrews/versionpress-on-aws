<?php
if ( empty ( getenv('WORDPRESS_DB_SECRET_ID') ) ) {
  // if the secret id isn't set, don't install this approach
  return;
}
/*
  Plugin Name: Extended wpdb to use AWS Secrets Manager credentials
  Description: Get the database username/password from an AWS Secrets Manager
  Version: 1.0
  Author: Craig Andrews
*/
class wpdb_aws_secrets_manager_extended extends wpdb {
 /**
  * Path to the cache file
  *
  * @var string
  */
  private $secretCacheFile;

  public function __construct() {
    $this->dbname     = defined( 'DB_NAME' ) ? DB_NAME : '';
    $this->dbhost     = defined( 'DB_HOST' ) ? DB_HOST : '';
    $this->secretCacheFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . md5(getenv('WORDPRESS_DB_SECRET_ID'));
    $this->_load_credentials();
    parent::__construct( $this->dbuser, $this->dbpassword, $this->dbname, $this->dbhost );
  }

  public function db_connect( $allow_bail = true ) {
    $ret = parent::db_connect( false );
    if (! $ret ) {
      // connection failed, refresh the credentials
      $this->_refresh_credentials();
      $ret = parent::db_connect( $allow_bail );
    }
    return $ret;
  }

 /**
  * Load the credentials from cached storage
  * If no credentials are cached, refresh credentials
  */
  private function _load_credentials() {
    if ( file_exists ( $this->secretCacheFile ) ) {
      $data = json_decode ( file_get_contents ( $this->secretCacheFile ) );
      $this->dbuser = $data->username;
      $this->dbpassword = $data->password;
    } else {
      $this->_refresh_credentials();
    }
  }

 /**
  * Refresh the credentials from Secrets Mananager
  * and write to cached storage
  */
  private function _refresh_credentials() {
    exec('aws secretsmanager get-secret-value --secret-id ' . escapeshellarg(getenv('WORDPRESS_DB_SECRET_ID')) . ' --query SecretString --output text > ' . escapeshellarg($this->secretCacheFile), $retArr, $status);
    chmod($this->secretCacheFile, 0600); // Read and write for owner, nothing for everybody else
    if ( $status != 0 ) {
      $this->bail("Could not refresh the AWS Secrets Manager secret");
      die();
    }
    $this->_load_credentials();
  }
}

global $wpdb;
$wpdb = new wpdb_aws_secrets_manager_extended();
