<?php
require '../wp-config.php';

if(defined('VP_PULL_SECRET') && !empty(VP_PULL_SECRET)){
	$secret = $_POST['SECRET'];
	if(empty($secret)){
		http_response_code(401);
	}else{
		if($secret == VP_PULL_SECRET){
			ob_start();
			header("Content-Type: text/plain");
			system("cd .. && vp-pull", $retval);
			http_response_code( $retval == 0? 200 : 500);
			ob_end_flush();
		}else{
			http_response_code(403);
		}
	}
}else{
	http_response_code(404);
}

