#!/bin/bash
# This file's name must start with apache2-*, see https://github.com/docker-library/wordpress/issues/205
set -Eeuo pipefail
cd /var/www/html
wp config set WP_ENV "${WP_ENV}" --type=constant
wp config set VP_ENVIRONMENT "${WP_ENV}" --type=constant
wp config set VP_PULL_SECRET "${VP_PULL_SECRET-}" --type=constant

cp /usr/local/db.php wp-content/
if [[ ! -f wp-content/.gitignore ]]; then
	echo "db.php" > wp-content/.gitignore
fi

if [[ -d wp-content/vpdb && $(ls -A wp-content/vpdb) ]]; then
	if ! wp core is-installed || [[ "$(wp option get siteurl)" != "${SITE_URL}" ]] ; then
		if [[ ! -d wp-content/plugins/versionpress ]]; then
			unzip -oq /usr/src/versionpress.zip -d wp-content/plugins/
			find wp-content/plugins/versionpress/ -type d -exec chmod +X {} \;
		fi
		wp vp restore-site --yes --siteurl="${SITE_URL}" --require=wp-content/plugins/versionpress/src/Cli/vp.php
		wp option update _transient_vp_commit "$(git rev-parse HEAD)" --quiet
	fi
fi
if ! wp core is-installed; then
	wp core install --url="${SITE_URL}" --title=Example --admin_user=admin --admin_email=admin@example.com --admin_password=password --skip-email
fi
cp /usr/local/vp-pull.php wp-content/
cp /usr/local/health.php wp-content/
if ! wp plugin is-installed versionpress; then
	wp plugin install ../../../usr/src/versionpress.zip --force --activate
	wp vp activate --yes
fi
if ! wp plugin is-active versionpress; then
	wp plugin activate versionpress
	wp vp activate --yes
fi
if [[ $(wp plugin get versionpress --field=version) != "${VERSIONPRESS_VERSION}" ]]; then
	wp vp update ../../../usr/src/versionpress.zip
fi

# Patches to apply to VersionPress
VP_COMMITS=(
	"5ca5fa73436b574e806ed15245be471e4b62abda"
	"5aa3b35f7f89640fad59d8c63f654e6b82ef446e"
	"c26a3e50206fcc7d4dd4efff79fbdf55665e78c3"
	"bab3ffb0318579335a9d6de99dd6a545b729a0af"
)
for commit in "${VP_COMMITS[@]}"; do
	if [[ ! -f "wp-content/plugins/versionpress/${commit}.applied" ]]; then
		# don't have a record of the patch having previously been applied
		patchfile=$(mktemp)
		curl -Ss -o "${patchfile}" "https://github.com/versionpress/versionpress/commit/${commit}.patch"
		if ! patch -d wp-content -R -p1 -s -f --dry-run < "${patchfile}" > /dev/null 2>&1 ; then
			# patch didn't cleanly reverse apply, so it hasn't been applied manually outside of our tracking
			patch -d wp-content -p1 -s -f < "${patchfile}"
		fi
		touch "wp-content/plugins/versionpress/${commit}.applied"
		rm "${patchfile}"
	fi
done

if [[ "$(wp option get _transient_vp_commit --quiet | tr -d '\n')" != "$(git rev-parse HEAD)" ]]; then
	wp vp apply-changes
	wp option update _transient_vp_commit "$(git rev-parse HEAD)" --quiet
fi

apache2-foreground

