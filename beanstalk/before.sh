#!/bin/bash
set -Eeuo pipefail
if [[ -z "${OWNER_UID-}" ]]; then
  OWNER_UID=$(stat -c '%u' /var/www/html/)
  if [ "${OWNER_UID}" -eq 0 ]; then
    echo "OWNER_UID must be set to the uid of the owner of the directory."
    exit 1
  fi
fi
if [[ "${OWNER_UID}" != "$(id -u www-data)" ]]; then
  usermod --non-unique --uid "${OWNER_UID}" www-data
fi
[ -d ~www-data/.ssh ] || mkdir -m 700 ~www-data/.ssh && chown www-data ~www-data/.ssh

KNOWN_HOSTS="${KNOWN_HOSTS-}"
KNOWN_HOSTS="${KNOWN_HOSTS//\\n/$'\n'}"
if [[ -n "${KNOWN_HOSTS}" && ! $( sudo -H -E -u www-data bash -c 'grep -sq "${KNOWN_HOSTS}" ~/.ssh/known_hosts' ) ]]; then
  sudo -H -E -u www-data bash -c 'echo "${KNOWN_HOSTS}" >> ~/.ssh/known_hosts'
fi
SSH_PRIVATE_KEY="${SSH_PRIVATE_KEY-}"
SSH_PRIVATE_KEY="${SSH_PRIVATE_KEY//\\n/$'\n'}"
if [[ -n "${SSH_PRIVATE_KEY}" ]]; then
  sudo -H -E -u www-data bash -c 'echo "${SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa'
  sudo -H -E -u www-data bash -c "chmod 600 ~/.ssh/id_rsa"
  sudo -H -E -u www-data bash -c "ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub"
  sudo -H -E -u www-data bash -c "chmod 644 ~/.ssh/id_rsa.pub"
fi
if [[ ! -d /var/www/html/.git && -n "${GIT_URL-}" ]]; then
  sudo -H -E -u www-data git clone --single-branch --branch "${GIT_BRANCH:-master}" "${GIT_URL}" /var/www/html/
fi
if [[ -d /var/www/html/.git && -n "${GIT_URL-}" && "$(git -C /var/www/html/ config remote.origin.url)" != "${GIT_URL}" ]]; then
  if sudo -H -E -u www-data git -C /var/www/html/ config remote.origin.url > /dev/null; then
    sudo -H -E -u www-data git -C /var/www/html/ remote set-url origin "${GIT_URL}"
    sudo -H -E -u www-data git -C /var/www/html/ fetch origin
  else
    sudo -H -E -u www-data git -C /var/www/html/ remote add -f -t "${GIT_BRANCH}" origin "${GIT_URL}"
  fi
fi
if [[ -d /var/www/html/.git && -n "${GIT_BRANCH-}" && "$(git -C /var/www/html/ rev-parse --abbrev-ref HEAD)" != "${GIT_BRANCH}" ]]; then
  sudo -H -E -u www-data git -C /var/www/html/ remote set-branches origin "${GIT_BRANCH}"
  sudo -H -E -u www-data git -C /var/www/html/ fetch origin "${GIT_BRANCH}"
  sudo -H -E -u www-data git -C /var/www/html/ checkout -f -B "${GIT_BRANCH}" origin/"${GIT_BRANCH}"
fi
if [[ -d /var/www/html/.git && -n "${GIT_BRANCH-}" && "$(git -C /var/www/html/ rev-parse --abbrev-ref --symbolic-full-name @\{u\})" != "origin/${GIT_BRANCH}" ]]; then
  sudo -H -E -u www-data git -C /var/www/html/ branch --set-upstream-to origin/"${GIT_BRANCH}"
fi
# Enable the untracked cache if possible
# Untracked cache speeds up commands that involve determining untracked files such as `git status`
# And `git status` is run a lot
# See https://git-scm.com/docs/git-update-index#_untracked_cache
if sudo -H -E -u www-data git -C /var/www/html/ update-index --test-untracked-cache; then
  sudo -H -E -u www-data git -C /var/www/html/ config core.untrackedCache true
else
  sudo -H -E -u www-data git -C /var/www/html/ config core.untrackedCache false
fi
# For security, unset variables
unset GIT_URL GIT_BRANCH KNOWN_HOSTS SSH_PRIVATE_KEY
exec "$@"

