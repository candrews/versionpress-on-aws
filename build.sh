#!/bin/bash
set -Eeuo pipefail
if [ -z "${S3_BUCKET_NAME-}" ]; then
  echo "The S3_BUCKET_NAME environment variable must be set to the S3 bucket name of the beanstalk app"
  exit 1
fi

#ensure the shell scripts are valid
shellcheck ./*.sh ./*/*.sh beanstalk/git-hooks/*

cd "$(dirname "$0")/beanstalk"

#make sure Dockerrun.aws.json is valid json
jq 'empty' Dockerrun.aws.json

# Use a fixed date so that regardless of when the archive is made, as long as the files are the same, the archive will be the same
ARCHIVE_DATE="1970-01-01"
find . -newermt "${ARCHIVE_DATE}" -print0 | xargs -0r touch --no-dereference --date="${ARCHIVE_DATE}"
rm -rf ./beanstalk-app.zip
[ -d ../target ] || mkdir ../target
zip -X -r ../target/beanstalk-app.zip .
cd .. || die
HASH=$(sha1sum target/beanstalk-app.zip | awk '{ print $1 }')
S3_KEY="beanstalk-versions/${HASH}/beanstalk-app.zip"
S3_URL="s3://${S3_BUCKET_NAME}/${S3_KEY}"
echo "${S3_URL}" > target/beanstalk-app-s3-url
cfn-flip -c -j cloudformation.yml | jq "del(.Resources |.[] |.Metadata .cfn_nag)|.Parameters.BeanstalkSourceBundleS3Bucket.Default=\"${S3_BUCKET_NAME}\"|.Parameters.BeanstalkSourceBundleS3Key.Default=\"${S3_KEY}\"" -c > target/cloudformation.json
echo "Success! Upload target/beanstalk-app.zip to ${S3_URL}"

