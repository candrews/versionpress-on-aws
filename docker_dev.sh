#!/usr/bin/env bash
set -Eeuo pipefail

cd "$(dirname "$0")"

if stat -c"%U" /dev/null >/dev/null 2>/dev/null ; then
  # GNU stat
  OWNER_UID=$(stat -c '%u' ../)
else
  # BSD stat
  OWNER_UID=$(stat -f '%u' ../)
fi
export OWNER_UID
docker-compose up --build
docker-compose down

